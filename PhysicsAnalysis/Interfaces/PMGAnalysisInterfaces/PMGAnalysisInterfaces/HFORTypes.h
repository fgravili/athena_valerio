#ifndef PMGANALYSISINTERFACES_HFORTYPES
#define PMGANALYSISINTERFACES_HFORTYPES

// Basic definitions for HFOR
enum HFORType{
  isBB = 0, isCC = 1, isC = 2, isLight = 3, kill = 4, noType = 5
};

#endif // !PMGANALYSISINTERFACES_HFORTYPES