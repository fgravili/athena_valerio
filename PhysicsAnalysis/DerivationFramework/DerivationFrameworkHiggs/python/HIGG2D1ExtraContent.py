# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#Content included in addition to the Smart Slimming Content

HIGG2D1ExtraContent=[
    "Muons.ptconecoreTrackPtrCorrection.ptvarcone40_LooseTTVA_pt500.ptvarcone30_LooseTTVA_pt500.ptvarcone20_LooseTTVA_pt500.ptvarcone40_TightTTVA_pt1000.ptvarcone30_TightTTVA_pt1000.ptvarcone20_TightTTVA_pt1000.ptvarcone40_TightTTVA_pt500.ptvarcone30_TightTTVA_pt500.ptvarcone20_TightTTVA_pt500.neflowisol20.neflowisol30.neflowisol40",
    "AntiKt4EMTopoJets.JetEMScaleMomentum_pt.JetEMScaleMomentum_eta.JetEMScaleMomentum_phi.JetEMScaleMomentum_m",
    "TauJets.IsTruthMatched.truthJetLink.truthParticleLink",
    "CaloCalTopoClusters.rawM.rawE.rawEta.rawPhi.calM.calE.calPhi.calEta.e_sampl.phi_sampl.eta_sampl",
    "Electrons.topoetcone30ptCorrection.topoetcone40ptCorrection.ptvarcone30.ptvarcone30_TightTTVA_pt500.ptvarcone20_TightTTVA_pt500.ptcone20_TightTTVA_pt500"
    ]

HIGG2D1ExtraContentTruth=[
    "TruthEvents.PDFID1.PDFID2.PDGID1.PDGID2.Q.X1.X2.XF1.XF2.weights.crossSection",
    "TruthParticles.barcode.prodVtxLink.decayVtxLink.status.pdgId.e.m.px.py.pz",
    "TruthVertices.barcode.id.incomingParticleLinks.outgoingParticleLinks"
    ]

HIGG2D1ExtraContainers=[]

HIGG2D1ExtraContainersTruth=[
    "MET_Truth",
    "MET_TruthRegions",
    "TruthElectrons",
    "TruthMuons",
    "TruthTaus",
    "TruthPhotons",
    "TruthNeutrinos",
    "TruthTop",
    "TruthBSM",
    "TruthBoson"
    ]
