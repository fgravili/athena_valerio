# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

AntiKt10LCTopoCSSKSoftDropBeta100Zcut10JetsCPContent = [
"AntiKt10LCTopoCSSKSoftDropBeta100Zcut10Jets",
"AntiKt10LCTopoCSSKSoftDropBeta100Zcut10JetsAux.pt.eta.phi.m.constituentLinks.ConstituentScale.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m.InputType.AlgorithmType.SizeParameter.TransformType.RClus.PtFrac.ECF1.ECF2.ECF3.Tau1_wta.Tau2_wta.Tau3_wta.Split12.Split23.NTrimSubjets.Parent.GhostAntiKt2TrackJet.TrackSumPt.TrackSumMass.DetectorEta.Qw.PlanarFlow.FoxWolfram2.FoxWolfram0.Angularity.Aplanarity.KtDR.ZCut12.ExKt2SubJets.ExKt3SubJets.ExCoM2SubJets",
]
