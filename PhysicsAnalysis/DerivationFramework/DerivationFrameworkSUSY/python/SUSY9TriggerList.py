
triggersBPhys = [
#'HLT_2mu4_bJpsimumu',
#'HLT_2mu4_bJpsimumu_Lxy0',
#'HLT_2mu4_bJpsimumu_L1BPH-2M9-2MU4_BPH-0DR15-2MU4',
#'HLT_2mu4_bJpsimumu_Lxy0_L1BPH-2M9-2MU4_BPH-0DR15-2MU4',
#'HLT_mu6_mu4_bJpsimumu',
#'HLT_mu6_mu4_bJpsimumu_Lxy0',
#'HLT_mu6_mu4_bJpsimumu_L1BPH-2M9-MU6MU4_BPH-0DR15-MU6MU4',
#'HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH-2M9-MU6MU4_BPH-0DR15-MU6MU4',
#'HLT_2mu6_bJpsimumu',
#'HLT_2mu6_bJpsimumu_Lxy0',
#'HLT_2mu6_bJpsimumu_L1BPH-2M9-2MU6_BPH-2DR15-2MU6',
#'HLT_2mu6_bJpsimumu_Lxy0_L1BPH-2M9-2MU6_BPH-2DR15-2MU6',
#'HLT_mu10_mu6_bJpsimumu',
#'HLT_mu10_mu6_bJpsimumu_Lxy0',
#'HLT_2mu10_bJpsimumu',
#'HLT_2mu4_bUpsimumu',
#'HLT_2mu4_bUpsimumu_L1BPH-7M15-2MU4_BPH-0DR24-2MU4',
#'HLT_mu6_mu4_bUpsimumu',
#'HLT_mu6_mu4_bUpsimumu_L1BPH-8M15-MU6MU4_BPH-0DR22-MU6MU4',
#'HLT_2mu6_bUpsimumu',
#'HLT_2mu6_bUpsimumu_L1BPH-8M15-2MU6_BPH-0DR22-2MU6',
#'HLT_mu10_mu6_bUpsimumu',
#'HLT_2mu10_bUpsimumu',
#'HLT_mu4_bJpsi_Trkloose',
#'HLT_mu6_bJpsi_Trkloose',
#'HLT_mu10_bJpsi_Trkloose',
#'HLT_mu18_bJpsi_Trkloose'
'HLT_.*Jpsi.*',
'HLT_.*Upsi.*'
#'HLT_.*mu.*Jpsi.*',
#'HLT_.*mu.*Upsi.*'
#'HLT_.*mu.*'
]

triggersNavThin=triggersBPhys

