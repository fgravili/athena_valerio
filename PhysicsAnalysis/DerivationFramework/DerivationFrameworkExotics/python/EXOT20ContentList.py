# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

EXOT20SmartCollections = [
    "Muons",
    "AntiKt4EMTopoJets",
    "BTagging_AntiKt4EMTopo",
    "MET_Reference_AntiKt4EMTopo",
    "InDetTrackParticles",
    "PrimaryVertices"
]

EXOT20AllVariables = [
    "TruthVertices",
    "TruthParticles",
    "MSonlyTracklets",
    "MuonSpectrometerTrackParticles",
    "ExtrapolatedMuonTrackParticles",
    "CombinedMuonTrackParticles"
]
