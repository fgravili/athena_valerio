# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

EXOT13SmartContent = [
    "InDetTrackParticles",
    "Electrons",
    "Muons",
    "PrimaryVertices",
    "MET_Reference_AntiKt4EMTopo",
    "AntiKt4EMTopoJets",
    "AntiKt4TruthJets"
]

EXOT13AllVariablesContent = [
    "MuonSpectrometerTrackParticles",
    "ExtrapolatedMuonTrackParticles",
    "METAssoc_AntiKt4EMTopo",
    "MET_Core_AntiKt4EMTopo",
    "MET_Truth",
    "TruthEvents",
    "TruthVertices",
    "TruthParticles"
]

EXOT13ExtraVariables = [
    "AntiKt4EMTopoJets.EMfrac.Width.Timing",
    "Muons.allAuthors.rpcHitTime.rpcHitIdentifier.rpcHitPositionX.rpcHitPositionY.rpcHitPositionZ",
    "MuonSegments.x.y.z.px.py.pz"
    #"CaloCalTopoClusters.e_sampl.calM.calE.calEta.calPhi"
]

EXOT13UnslimmedContent = []

