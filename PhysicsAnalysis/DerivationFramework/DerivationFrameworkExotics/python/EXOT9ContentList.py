# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

EXOT9Content = []

EXOT9AllVariables = [
    "TruthParticles",
    "TruthEvents",
    "TruthVertices",
    "MET_Truth",
    "ExtrapolatedMuonTrackParticles",
    "CombinedMuonTrackParticles"
]

EXOT9SmartCollections = [
    "Photons",
    "TauJets",
    "Electrons",
    "Muons",
    "MET_Reference_AntiKt4EMTopo",
    "MET_Reference_AntiKt4EMPFlow",
    "BTagging_AntiKt4EMTopo",
    "BTagging_AntiKt4EMPFlow",
    "AntiKt4EMTopoJets",
    "AntiKt4EMPFlowJets",
    "PrimaryVertices",
    "InDetTrackParticles"
]

EXOT9Extravariables = [
    "Muons.rpcHitTime.meanDeltaADCCountsMDT.MeasEnergyLoss",
    "InDetTrackParticles.numberOfIBLOverflowsdEdx.TRTdEdxUsedHits.TRTdEdx.numberOfTRTHighThresholdHitsTotal.numberOfTRTXenonHits.numberOfUsedHitsdEdx.pixeldEdx"
]
