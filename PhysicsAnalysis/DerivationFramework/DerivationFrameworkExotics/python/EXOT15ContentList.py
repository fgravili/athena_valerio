# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

EXOT15SmartContent = [
    "Muons",
    "Electrons",
    "Photons",
    "AntiKt4EMTopoJets",
    "InDetTrackParticles",
    "PrimaryVertices",
    "MET_Reference_AntiKt4EMTopo",
    "BTagging_AntiKt4EMTopo"
]

EXOT15AllVariablesContent = [
    "BTagging_AntiKt4EMTopo",
    "TruthEvents",
    "TruthVertices",
    "TruthParticles",
    "AntiKt4TruthJets",
    "MuonSegments",
    "MSDisplacedVertex",
    "MSonlyTracklets",
    "AntiKt4EMTopoJets",
    "CaloCalTopoClusters",
    "MuonTruthParticles",
    "egammaTruthParticles",
    "CombinedMuonTrackParticles",
    "MET_Truth",
    "ExtrapolatedMuonTrackParticles",
    "LVL1MuonRoIs",
    "HLT_xAOD__JetContainer_SplitJet",
    "HLT_xAOD__JetContainer_a4tcemjesPS",
    "HLT_xAOD__ElectronContainer_egamma_Electrons",
    "HLT_xAOD__TrackParticleContainer_InDetTrigTrackingxAODCnv_Tau_IDTrig",
    "HLT_xAOD__TrackParticleContainer_InDetTrigTrackingxAODCnv_Muon_IDTrig",
    "HLT_xAOD__TrigCompositeContainer_MuonRoICluster"
]
