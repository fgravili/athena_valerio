Atlas Derivation Framework
==========================

Welcome to the ATLAS derivation framework! For more comprehensive documentation
see the [twiki][1].

[1]: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DerivationFramework