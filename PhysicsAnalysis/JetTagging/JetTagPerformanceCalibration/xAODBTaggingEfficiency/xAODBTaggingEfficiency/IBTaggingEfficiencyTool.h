// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/
#pragma message "The IBTaggingEfficiencyTool has moved to FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h . Please update your code."
#include <FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h>

