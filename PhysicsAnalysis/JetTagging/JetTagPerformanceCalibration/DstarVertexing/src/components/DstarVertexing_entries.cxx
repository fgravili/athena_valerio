
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "DstarVertexing/DstarVertexing.h"

DECLARE_TOOL_FACTORY( DstarVertexing )

DECLARE_FACTORY_ENTRIES( DstarVertexing ) 
{
  DECLARE_TOOL( DstarVertexing );
}
