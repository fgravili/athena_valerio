#include "GaudiKernel/DeclareFactoryEntries.h"

#include "DiTauMassTools/MissingMassTool.h"

DECLARE_TOOL_FACTORY( MissingMassTool )

DECLARE_FACTORY_ENTRIES( DiTauMassTools ) 
{
  DECLARE_TOOL( MissingMassTool );
}
