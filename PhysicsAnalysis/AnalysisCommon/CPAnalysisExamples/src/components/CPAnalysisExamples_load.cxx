// $Id: CPAnalysisExamples_load.cxx 299890 2014-03-29 08:54:38Z krasznaa $

// Gaudi/Athena include(s):
#include "GaudiKernel/LoadFactoryEntries.h"

// Declare the library to the infrastructure:
LOAD_FACTORY_ENTRIES( CPAnalysisExamples )
