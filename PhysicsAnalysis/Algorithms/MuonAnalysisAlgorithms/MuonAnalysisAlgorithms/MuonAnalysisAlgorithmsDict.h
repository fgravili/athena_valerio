/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef MUON_ANALYSIS_ALGORITHMS__MUON_ANALYSIS_ALGORITHMS_DICT_H
#define MUON_ANALYSIS_ALGORITHMS__MUON_ANALYSIS_ALGORITHMS_DICT_H

#include <MuonAnalysisAlgorithms/MuonCalibrationAndSmearingAlg.h>
#include <MuonAnalysisAlgorithms/MuonIsolationAlg.h>
#include <MuonAnalysisAlgorithms/MuonEfficiencyScaleFactorAlg.h>
#include <MuonAnalysisAlgorithms/MuonTriggerEfficiencyScaleFactorAlg.h>

#endif
