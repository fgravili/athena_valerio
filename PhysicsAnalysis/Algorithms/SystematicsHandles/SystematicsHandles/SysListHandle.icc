/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


//
// includes
//

#include <cassert>

//
// method implementations
//

namespace CP
{
  inline MsgStream& SysListHandle ::
  msg () const
  {
    assert (m_msg != nullptr);
    return *m_msg;
  }



  inline MsgStream& SysListHandle ::
  msg (MSG::Level lvl) const
  {
    assert (m_msg != nullptr);
    *m_msg << lvl;
    return *m_msg;
  }



  template<typename T> SysListHandle ::
  SysListHandle (T *owner, const std::string& propertyName,
                     const std::string& propertyDescription)
    : m_evtStoreGetter ([owner] () {return &*owner->evtStore();})
    , m_msg (&owner->msg())
  {
    owner->declareProperty (propertyName, m_systematicsListName,
                            propertyDescription);
    owner->declareProperty (propertyName + "Regex", m_affectingRegex, "affecting systematics for " + propertyDescription);
  }



  inline bool SysListHandle ::
  isInitialized () const noexcept
  {
    return m_isInitialized;
  }
}
