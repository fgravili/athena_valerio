/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef jetsubstructuremomenttools_ecfhelper_header
#define jetsubstructuremomenttools_ecfhelper_header

#include <algorithm>
#include <string>
#include <cmath>

std::string GetBetaSuffix(float beta);

#endif
