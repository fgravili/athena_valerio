
#include "GaudiKernel/DeclareFactoryEntries.h"

// Local include(s):
#include "JetJvtEfficiency/JetJvtEfficiency.h"

DECLARE_NAMESPACE_TOOL_FACTORY( CP, JetJvtEfficiency )

DECLARE_FACTORY_ENTRIES( JetJvtEfficiency ) {

   DECLARE_NAMESPACE_TOOL( CP, JetJvtEfficiency )

}
