#
# File specifying the location of Sherpa to use.
#

set( SHERPA_LCGVERSION 2.2.4 )
set( SHERPA_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/sherpa/${SHERPA_LCGVERSION}/${LCG_PLATFORM} )
