#
# File specifying the location of Rivet to use.
#

set( RIVET_LCGVERSION 2.5.3 )
set( RIVET_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/rivet/${RIVET_LCGVERSION}/${LCG_PLATFORM} )
